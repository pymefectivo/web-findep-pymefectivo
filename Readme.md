# Web Pymefectivo

Creación de página Web de la Financiera “PyMEfectivo”.

## Secciones

 * Home
 * Preguntas frecuentes
 * Nosotros
 * CONDUSEF
 * Contacto
 * Aviso de privacidad
 * Términos y condiciones

### Instalación

Instalar nodejs 10.x de:
https://nodejs.org/dist/latest-v10.x/

`npm install`

instalación super usuario:

`sudo npm install`

instalar gulp

`npm install --global gulp-cli`

### Desarrollo

Compilar proyecto

`gulp`

### Despliegue

Para desplegar la aplicacion hay que mandar el contenido de la carpeta `dist` a `deploy/www`

En la consolla nos ubicamos en la carpeta deploy y ejecutamos el comando

`gcloud app deploy`


### Tecnologías

* Nodejs 10.x
* Javascript
* Gulp 3.9
* Stylus
* CSS
* HTML