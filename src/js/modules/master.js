window.formID;
$(document).ready(function () {

	window.reCaptchaResponse = function (token) { 
		$(document).trigger('captchaResponse-' + formID, token)
	}

	// SLIDER TOP
	var mySwiper = new Swiper('.swiper-container', {
		direction: 'horizontal',
		loop: true,
		speed: 1000,
		pagination: {
			el: '.swiper-pagination',
			clickable: true
		},
		autoplay: {
			delay: 6000,
			disableOnInteraction: false
		},
		breakpoints: {
			300: {
				autoHeight: true,
			}

		}
	});

	var mySwiper02 = new Swiper('.swiper-container-requisitos', {
		direction: 'horizontal',
		slidesPerView: 'auto',
		spaceBetween: 30,
		centeredSlides: true,
		grabCursor: true,
		loop: false,
		speed: 1000,
		breakpoints: {
			370: {
				slidesPerView: 'auto',
				centeredSlides: false,
				spaceBetween: 30
			}

		}
	});

	// NAV BEHAVIOR
	$('.nav_button').click(function (e) {
		e.preventDefault();
		$('.nav-icon').toggleClass('open');
		$('body').toggleClass('no-scroll');
		$('.header').toggleClass('nav-movile');
	});

	// ACCORDION
	var accordionTitle = $('.accordion_title');
	accordion(accordionTitle);
	
	
});


function validEmail(email) {
	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	if (!emailReg.test(email)) {
		return false;
	} else {
		return true;
	}
}

function allLetter(inputTxt) {
	var letters = /^[a-zA-Z áéíóúñ ÁÉÍÓÚÑ]+$/;
	if (!letters.test(inputTxt)) {
		return false;
	} else {
		return true;
	}
}

function allNumber(inputNum) {
	var numbers = /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s./0-9]*$/;
	if (!numbers.test(inputNum)) {
		return false;
	} else {
		return true;
	}
}

function allNumberCP(inputNumCP) {
	var numbers =  /^[0-9]+/;
	if (!numbers.test(inputNumCP)) {
		return false;
	} else {
		return true;
	}
}

function gaContactForm(avance){
	dataLayer.push({
		'avance': avance, // 'avance' será una variable que contenga "inicio", "llenado" o "enviado".
		'event': 'Formulario_Contacto'
	});
	
}

function accordion(el) {
	if (!detectmob()) {
		$('.accordion_list .chevron').hide();
		$(el).next().slideDown();
		$(el).on('click', function () {
			$(this).next().stop();
		});
	}

	if (detectmob()) {
		$(el).next().slideUp();
		$('.accordion_list .chevron').show();
		$(el).on('click', function () {
			$(this).next().slideToggle('fast');
			$(this).children('.chevron').stop().toggleClass('top-open');
		});
	}
	
}

function cleanFeedback(){
	$('.msg_form').text('').removeClass('show').removeAttr('style');
}

function fill(t) {
	t.addClass('fill');
}

function clearFill(t) {
	t.removeClass('fill');
}

// DETECT MOBILE
; function detectmob() { if (navigator.userAgent.match(/Touch/i) || navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i) || navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/BlackBerry/i) || navigator.userAgent.match(/Windows Phone/i)) { return true } else { return false } }