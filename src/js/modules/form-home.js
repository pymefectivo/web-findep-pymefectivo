
var dataCP = [
	{
		id: '',
		text: ''
	},
	{
		id: '50000',
		text: '50000'
	},
	{
		id: '50009',
		text: '50009'
	},
	{
		id: '50010',
		text: '50010'
	},
	{
		id: '50016',
		text: '50016'
	},
	{
		id: '50017',
		text: '50017'
	},
	{
		id: '50018',
		text: '50018'
	},
	{
		id: '50019',
		text: '50019'
	},
	{
		id: '50020',
		text: '50020'
	},
	{
		id: '50021',
		text: '50021'
	},
	{
		id: '50025',
		text: '50025'
	},
	{
		id: '50026',
		text: '50026'
	},
	{
		id: '50030',
		text: '50030'
	},
	{
		id: '50039',
		text: '50039'
	},
	{
		id: '50040',
		text: '50040'
	},
	{
		id: '50050',
		text: '50050'
	},
	{
		id: '50060',
		text: '50060'
	},
	{
		id: '50069',
		text: '50069'
	},
	{
		id: '50070',
		text: '50070'
	},
	{
		id: '50075',
		text: '50075'
	},
	{
		id: '50076',
		text: '50076'
	},
	{
		id: '50080',
		text: '50080'
	},
	{
		id: '50090',
		text: '50090'
	},
	{
		id: '50098',
		text: '50098'
	},
	{
		id: '50100',
		text: '50100'
	},
	{
		id: '50110',
		text: '50110'
	},
	{
		id: '50120',
		text: '50120'
	},
	{
		id: '50122',
		text: '50122'
	},
	{
		id: '50123',
		text: '50123'
	},
	{
		id: '50130',
		text: '50130'
	},
	{
		id: '50140',
		text: '50140'
	},
	{
		id: '50145',
		text: '50145'
	},
	{
		id: '50149',
		text: '50149'
	},
	{
		id: '50150',
		text: '50150'
	},
	{
		id: '50159',
		text: '50159'
	},
	{
		id: '50160',
		text: '50160'
	},
	{
		id: '50168',
		text: '50168'
	},
	{
		id: '50170',
		text: '50170'
	},
	{
		id: '50180',
		text: '50180'
	},
	{
		id: '50190',
		text: '50190'
	},
	{
		id: '50199',
		text: '50199'
	},
	{
		id: '50200',
		text: '50200'
	},
	{
		id: '50205',
		text: '50205'
	},
	{
		id: '50209',
		text: '50209'
	},
	{
		id: '50210',
		text: '50210'
	},
	{
		id: '50217',
		text: '50217'
	},
	{
		id: '50219',
		text: '50219'
	},
	{
		id: '50220',
		text: '50220'
	},
	{
		id: '50226',
		text: '50226'
	},
	{
		id: '50230',
		text: '50230'
	},
	{
		id: '50250',
		text: '50250'
	},
	{
		id: '50251',
		text: '50251'
	},
	{
		id: '50252',
		text: '50252'
	},
	{
		id: '50260',
		text: '50260'
	},
	{
		id: '50261',
		text: '50261'
	},
	{
		id: '50270',
		text: '50270'
	},
	{
		id: '50290',
		text: '50290'
	},
	{
		id: '50293',
		text: '50293'
	}
];

var URL_API_LEADS = 'https://pymefectivo-service-dot-findep-desarrollo-170215.appspot.com/api/v1/leads';
var submitClickHome = false;
var successHome = false;
var inputsHome = $('#solicitud_form .field input');
var inputCP = $('#solicitud_cp');
var inputCPVal = '';
var selectsHome = $('#solicitud_form .field select');
var btnSubmitHome = $('#solicitud_form .btn_submit');
var filledHome = false;
var cpURL = "https://api-pymefectivo-dot-findep-desarrollo-170215.appspot.com/direcciones/obtenerColoniasCp/";


inputCP.on('input', function() {
	inputCPVal = $(this).val();
	console.log(inputCPVal);
});


// select2 only numbers
$(document).on('input', '.select2-search__field', function () {
	var value = $(this).val();
	if (value.length > 5) {
		$(this).val(value.slice(0, 5));
	}
});


$('#solicitud_form .btn_submit').on('click', function (e) {
	e.preventDefault();
	submitClickHome = true;
	evalFormHome();
	if (successHome == true) {
		loader();
		formID = 'solicitud_form';
		window.grecaptcha.execute();
		submitClickHome = false;
	}
});

$('#solicitud_form .field').on('focusout', 'input, select', function (e) {

	if (submitClickHome && !successHome) {
		evalFormHome();
	}
	else {
		successHome = false;
	}

});

$(document).on('captchaResponse-solicitud_form', function (e, token) {
	$.ajax({
		type: 'POST',
		url: URL_API_LEADS,
		dataType: "JSON",
		contentType: "application/json; charset=utf-8",
		data: JSON.stringify({ 
			"name": $('#solicitud_nombre').val(),
			"email": $('#solicitud_email').val(),
			"phone": $('#solicitud_tel').val(),
			"postalCode": $('#solicitud_cp').val(),
			"gRecaptcha": token,
		}),
	})
	.done(successEvalHome)
	.fail(errorEvalHome)
	.always(function() { window.grecaptcha.reset() })
});

// disable sumbit at start
$(btnSubmitHome).attr('disabled', true);

inputsHome.keyup(function (t) {
	var t = $(this);
	'' != this.value ? fill(t) : clearFill(t);
	checkFilledHome();
});

selectsHome.change(function (t) {
	var t = $(this);
	'' != this.value ? fill(t) : clearFill(t);
	checkFilledHome();
});

inputsHome.blur(function (t) { 
	var t = $(this);
	'' != this.value ? evalInputHome() : t.parent().addClass('error');

	function evalInputHome(){
		var name = $('#solicitud_nombre').val();
		var email = $('#solicitud_email').val();
		var tel = $('#solicitud_tel').val();
		var cp = $('#solicitud_cp').val();
		
		if (t.val() == name) {
	
			if (name.length < 2) {
				$('#solicitud_nombre').parent().addClass('error');
				$('#solicitud_nombre').parent().removeClass('success');
			}
			else {
				if (!allLetter(name)) {
					$('#solicitud_nombre').parent().addClass('error');
					$('#solicitud_nombre').parent().removeClass('success');
				}
				else {
					$('#solicitud_nombre').parent().removeClass('error');
					$('#solicitud_nombre').parent().addClass('success');
				}
			}		
		}

		if (t.val() == email) {
			if (email.length < 1) {
				$('#solicitud_email').parent().addClass('error');
				$('#solicitud_email').parent().removeClass('success');
			} else {
				if (!validEmail(email)) {
					$('#solicitud_email').parent().addClass('error');
					$('#solicitud_email').parent().removeClass('success');
					
				}
				else {
					$('#solicitud_email').parent().removeClass('error');
					$('#solicitud_email').parent().addClass('success');
				}
			}
		}

		if (t.val() == tel) {
			if (tel.length < 10) {
				$('#solicitud_tel').parent().addClass('error');
				$('#solicitud_tel').parent().removeClass('success');
			}
			else {
				if (!allNumber(tel)) {
					$('#solicitud_tel').parent().addClass('error');
					$('#solicitud_tel').parent().removeClass('success');
				}
				else {
					$('#solicitud_tel').parent().removeClass('error');
					$('#solicitud_tel').parent().addClass('success');
				}
			}
		}

		if (t.val() == cp) {
			if (cp.length < 5) {
				$('#solicitud_cp').parent().addClass('error');
				$('#solicitud_cp').parent().removeClass('success');
			}
			else {
				if (!allNumber(cp)) {
					$('#solicitud_cp').parent().addClass('error');
					$('#solicitud_cp').parent().removeClass('success');
				}
				else {
					$.ajax ({
						url: cpURL + inputCPVal + '/',
						dataType: 'json',
						method: 'GET',
						timeout: 0,
						xhrFields: {
							WhiteCredentials: true
						},
						statusCode: {
							200: function(){
								console.log('success');
							},
							404: function(){
								console.log('ERROR!');
								$('#solicitud_cp')
								.parent()
								.add("<div class='select_msg '><p class='txt-bold'>¡Lo sentimos!</p> <p>Por el momento no contamos con cobertura en tu zona. Comunícate al <a href='tel:+528000609901' class='green'><span class='green bold'>800 060 9901</a> para más información.</p><div>")
							},
							500: function(){
								console.log('SERVER FAIL!');
								$('#solicitud_cp')
								.closest('.field')
								.append("<div class='select_msg '><p class='txt-bold'>¡Lo sentimos!</p> <p>Por el momento no contamos con cobertura en tu zona. Comunícate al <a href='tel:+528000609901' class='green'><span class='green bold'>800 060 9901</a> para más información.</p><div>")
								setTimeout(function () {
									$('.select_msg').slideOut();
								}, 3000);
							}
						}

					});
					$('#solicitud_cp').parent().removeClass('error');
					$('#solicitud_cp').parent().addClass('success');
				}
			}
		}
	}
});

selectsHome.change(function (t) {
	var t = $(this);
	'' != this.value ? evalSelectsSolicitud() : evalSelectsSolicitudEmpty();

	var cp = $('#solicitud_cp').val();

	function evalSelectsSolicitudEmpty() { 
		$('#solicitud_cp').closest('.field').addClass('error');
		$('#solicitud_cp').closest('.field').removeClass('success');
	}
	function evalSelectsSolicitud(){
		$('#solicitud_cp').closest('.field').addClass('success');
		$('#solicitud_cp').closest('.field').removeClass('error');
	}
});

var executedInicioHome = false;
var executedLlenadoHome = false;

function checkFilledHome() {

	var $filledHome = $('.fill').length;
	filledHome = false;

	($filledHome == 4) ? filledHome = true : filledHome = false;

	if (!executedInicioHome) {
		executedInicioHome = true;
		($filledHome == 1) ? gaHomeForm('inicio') : false;
	}

	if(filledHome){
		$(btnSubmitHome).removeAttr('disabled');
		$(btnSubmitHome).removeClass('btn_disabled');
		if (!executedLlenadoHome) {
			executedLlenadoHome = true;
			gaHomeForm('llenado');
		}
	}
	else{
		$(btnSubmitHome).attr('disabled', true);
		$(btnSubmitHome).addClass('btn_disabled');
	}
}

function evalFormHome(submitClickHome) {

	var name = $('#solicitud_nombre').val();
	var email = $('#solicitud_email').val();
	var tel = $('#solicitud_tel').val();
	var cp = $('#solicitud_cp').val();
	var nameEval = false;
	var emailEval = false;
	var telEval = false;
	var cpEval = false;

	$('.field').removeClass('error');

	if (name.length < 2) {
		$('#solicitud_nombre').parent().addClass('error');
	}
	else {
		if (!allLetter(name)) {
			$('#solicitud_nombre').parent().addClass('error');
		}
		else {
			$('#solicitud_nombre').parent().removeClass('error');
			$('#solicitud_nombre').parent().addClass('success');
			nameEval = true;
		}
	}

	if (email.length < 1) {
		$('#solicitud_email').parent().addClass('error');
	} else {
		if (!validEmail(email)) {
			$('#solicitud_email').parent().addClass('error');
		}
		else {
			$('#solicitud_email').parent().removeClass('error');
			$('#solicitud_email').parent().addClass('success');
			emailEval = true;
		}
	}
	if (tel.length < 10) {
		$('#solicitud_tel').parent().addClass('error');
	}
	else {
		if (!allNumber(tel)) {
			$('#solicitud_tel').parent().addClass('error');
		}
		else {
			$('#solicitud_tel').parent().removeClass('error');
			$('#solicitud_tel').parent().addClass('success');
			telEval = true;
		}
	}
	if (cp == '') {
		$('#solicitud_cp').closest('.field').addClass('error');
	}
	else {
		$('#solicitud_cp').closest('.field').addClass('success');
		cpEval = true;
	}
	if (nameEval && emailEval && telEval && cpEval) {
		submitClickHome = false;
		successHome = true;
	}
}
function loader() { 
	$('.msg_form').text('Enviando...').addClass('show');
}

function successEvalHome() {
	$('.msg_form').text('Datos enviados correctamente.').addClass('show');
	setTimeout(function () {
		inputsHome.val('');
		inputsHome.parent().removeClass('success');
		selectsHome.prop('selectedIndex', 0);
		selectsHome.closest('.field').removeClass('success');
		$(".select_CP").trigger('change.select2');
		$('.msg_form').text('').removeClass('show');
		cleanFeedback();
		$(btnSubmitHome).addClass('btn_disabled');
	}, 3000);
	successHome = false;
	$('.fill').removeClass('fill');
	$(btnSubmitHome).attr('disabled');
	gaHomeForm('enviado');
	executedInicioHome = false;
	executedLlenadoHome = false;
}

function errorEvalHome() { 
	$('.msg_form').text('Error al enviar datos').addClass('show')
	.css({
		'background': 'rgba(255, 0, 0, 0.15)',
		'color': '#ca4848'
	});
	setTimeout(function () {
		cleanFeedback();
	}, 3000);
}

function gaHomeForm(avanceHome){
	dataLayer.push({
		'avance': avanceHome,    // 'avance' será una variable que contenga ""llenado"" o ""enviado"".
		'event': 'Formulario_Registro'
	});
}
