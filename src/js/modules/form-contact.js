// CONTACT VALIDATION
var URL_API_CONTACTO = 'https://pymefectivo-service-dot-findep-desarrollo-170215.appspot.com/api/v1/contacts';
var submitClick = false;
var success = false;
var inputs = $('#contact_form .field input');
var selects = $('#contact_form .field select');
var btnSubmit = $('#contact_form .btn_submit')
var selected = false;
var filled = false;

$('#contact_form .btn_submit').on('click', function (e) {
	e.preventDefault();
	submitClick = true;
	evalFormContact();
	if (success == true) {
		loader();
		formID = 'contact_form';
		window.grecaptcha.execute();
		submitClick = false;
	}
});

$('#contact_form .field').on('focusout', 'input, select', function (e) {

	if (submitClick && !success) {
		evalFormContact();
	}
	else {
		success = false;
	}

});


// disable sumbit at start
$(btnSubmit).attr('disabled', true);

inputs.keyup(function (t) {
	var t = $(this);
	'' != this.value ? fill(t) : clearFill(t);
	checkFilled();
});

selects.change(function (t) {
	var t = $(this);
	'' != this.value ? fill(t) : clearFill(t);
	checkFilled();
});

inputs.blur(function (t) { 
	var t = $(this);
	'' != this.value ? evalInputContact() : t.parent().addClass('error');

	function evalInputContact(){
		var name = $('#nombre').val();
		var email = $('#email').val();
		var msg = $('#mensaje').val();

		
		if (t.val() == name) {
	
			if (name.length < 2) {
				$('#nombre').parent().addClass('error');
				$('#nombre').parent().removeClass('success');
			}
			else {
				if (!allLetter(name)) {
					$('#nombre').parent().addClass('error');
					$('#nombre').parent().removeClass('success');
				}
				else {
					$('#nombre').parent().removeClass('error');
					$('#nombre').parent().addClass('success');
				}
			}
		}

		if (t.val() == email) {
			if (email.length < 1) {
				$('#email').parent().addClass('error');
				$('#email').parent().removeClass('success');
			} else {
				if (!validEmail(email)) {
					$('#email').parent().addClass('error');
					$('#email').parent().removeClass('success');
				}
				else {
					$('#email').parent().removeClass('error');
					$('#email').parent().addClass('success');
				}
			}
		}

		if (t.val() == msg) {
			
			if (msg.length < 5) {
				$('#mensaje').parent().addClass('error');
				$('#mensaje').parent().removeClass('success');
			}
			else {
				$('#mensaje').parent().removeClass('error');
				$('#mensaje').parent().addClass('success');
			}
		}
	}
});

selects.change(function (t) {
	var t = $(this);
	'' != this.value ? evalSelectsContact() : evalSelectsContactEmpty();

	var topic = $('#tema').val();

	function evalSelectsContactEmpty() { 
		$('#tema').closest('.field').addClass('error');
		$('#tema').closest('.field').removeClass('success');
	}
	function evalSelectsContact(){
		$('#tema').closest('.field').addClass('success');
		$('#tema').closest('.field').removeClass('error');
	}
});

$(document).on('captchaResponse-contact_form', function (e, token) {
	$.ajax({
		type: 'POST',
		url: URL_API_CONTACTO,
		dataType: "JSON",
		contentType: "application/json; charset=utf-8",
		data: JSON.stringify({ 
			"name": $('#nombre').val(),
			"email": $('#email').val(),
			"topic": $('#tema option:selected').text(),
			"message": $('#mensaje').val(),
			"gRecaptcha": token,
		}),
	})
	.done(successEval)
	.fail(errorEval)
	.always(function() { window.grecaptcha.reset() })
});

var executedInicio = false;
var executedLlenado = false;
function checkFilled() {

	var $filled = $('.fill').length;
	filled = false;
	selected = false;

	($filled == 4) ? filled = true : filled = false;

	if (!executedInicio){
		executedInicio = true;
		($filled == 1) ? gaContactForm('inicio') : false;
	}

	($('.select').val() != '') ? selected = true : selected = false;

	if(filled && selected){
		$(btnSubmit).removeAttr('disabled');
		$(btnSubmit).removeClass('btn_disabled');
		if (!executedLlenado) {
			executedLlenado = true;
			gaContactForm('llenado');
		}
	}
	else{
		$(btnSubmit).attr('disabled', true);
		$(btnSubmit).addClass('btn_disabled');
	}
}

function evalFormContact(submitClick) {

	var name = $('#nombre').val();
	var email = $('#email').val();
	var topic = $('#tema').val();
	var msg = $('#mensaje').val();
	var nameEval = false;
	var emailEval = false;
	var topicEval = false;
	var msgEval = false;

	$('.field').removeClass('error');

	if (name.length < 2) {
		$('#nombre').parent().addClass('error');
	}
	else {
		if (!allLetter(name)) {
			$('#nombre').parent().addClass('error');
		}
		else {
			$('#nombre').parent().removeClass('error');
			$('#nombre').parent().addClass('success');
			nameEval = true;
		}
	}

	if (email.length < 1) {
		$('#email').parent().addClass('error');
	} else {
		if (!validEmail(email)) {
			$('#email').parent().addClass('error');
		}
		else {
			$('#email').parent().removeClass('error');
			$('#email').parent().addClass('success');
			emailEval = true;
		}
	}
	if (topic == '') {
		$('#tema').closest('.field').addClass('error');
	}
	else {
		$('#tema').closest('.field').addClass('success');
		topicEval = true;
	}
	if (msg.length < 5) {
		$('#mensaje').parent().addClass('error');
	}
	else {
		$('#mensaje').parent().removeClass('error');
		$('#mensaje').parent().addClass('success');
		msgEval = true;
	}
	if (nameEval && emailEval && topicEval && msgEval) {
		submitClick = false;
		success = true;
	}

}
function loader() { 
	$('.msg_form').text('Enviando...').addClass('show');
}

function successEval() {
	$('.msg_form').text('Datos enviados correctamente.').addClass('show');
	setTimeout(function () {
		inputs.val('');
		selects.prop('selectedIndex', 0);
		inputs.parent().removeClass('success');
		selects.closest('.field').removeClass('success');
		cleanFeedback();
		$(btnSubmit).addClass('btn_disabled');
	}, 3000);
	success = false;
	$('.fill').removeClass('fill');
	$(btnSubmit).attr('disabled');
	gaContactForm('enviado');
	executedInicio = false;
	executedLlenado = false;
}

function errorEval() { 
	$('.msg_form').text('Error al enviar datos').addClass('show')
	.css({
		'background': 'rgba(255, 0, 0, 0.15)',
		'color': '#ca4848'
	});
	setTimeout(function () {
		cleanFeedback();
	}, 3000);
}