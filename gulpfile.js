var gulp         = require('gulp'), // Gulp
	pug          = require('gulp-pug'), // Pug
	stylus       = require('gulp-stylus'), // Stylus

	nib          = require('nib'), //nib - plugin
	autoprefixer = require('autoprefixer-stylus'), // Autoprefixer
	rename       = require('gulp-rename'),
	cleanCSS     = require('gulp-clean-css'),

	concat       = require('gulp-concat'), // Concat
	uglify       = require('gulp-uglify'), // uglify
	plumber      = require('gulp-plumber'),

	browsersync  = require('browser-sync'), // Browser-Sync
	notify       = require('gulp-notify'),
	jeet         = require('jeet');

var dev_path =
	{
		styl: 'src/stylus/',
		js:   'src/js/',
		pug: 'src/pug/',
		img: 'src/img/'
	}

var build_path =
	{
		css:  'dist/css/',
		html: 'dist/',
		js:   'dist/js/',
		img:  'dist/img/'
	}

// Dev Tasks

// Compile Pug
gulp.task('pug', function(){
	return gulp.src([
			dev_path.pug + '*.pug',
			'!' + dev_path.pug + '_*.pug',
			'!' + dev_path.pug + 'includes/_*.pug'
		])
		.pipe(plumber())
		.pipe(pug({pretty: true}))
		.on('error', console.log)
		.pipe(gulp.dest(build_path.html))
		.pipe(notify({ message: 'HTML compiled!' }))
		.pipe(browsersync.reload({stream: true}));
});

// Export images
gulp.task('copy', function(){
	return gulp.src([ dev_path.img + '**/*'])
		.pipe(plumber())
		.on('error', console.log)
		.pipe(gulp.dest(build_path.img))
		.pipe(browsersync.reload({stream: true}));
});


// Compile Stylus
gulp.task('stylus',  function(){
	return gulp.src([
		dev_path.styl + 'master.styl'
		])
		.pipe(plumber())
		.pipe(stylus({
			compress: true,
			'include css': true,
			use: [
				nib(),
				jeet(),
				autoprefixer()
				]
		}))
		.on('error', console.log)
		.pipe(cleanCSS({compatibility: 'ie8'}))
		.pipe(rename('master.css'))
		.pipe(gulp.dest(build_path.css))
		.pipe(notify({ message: 'CSS compiled!' }))
		.pipe(browsersync.reload({stream: true}));
});

// JavaScript

//
// TRANSFER VENDOR FILES
//
// gulp.task('vendor', function(){
// 	gulp.src(dev_path.js + 'vendor/*')
// 		.pipe(concat('dependencies.min.js'))
// 		.pipe(uglify())
// 		.pipe(gulp.dest(dev_path.js + 'vendor/'))
// 		.pipe(notify({ message: 'Dependencies concatenated and minified!' }));
// });

gulp.task('js', function(){
	return gulp.src([ dev_path.js + 'vendor/*',  dev_path.js + 'modules/*'])
		.pipe(plumber())
		.on('error', console.log)
		.pipe(concat('master.min.js'))
		.pipe(uglify())
		.pipe(gulp.dest(build_path.js))
		.pipe(notify({ message: 'Javascript concatenated!' }))
		.pipe(browsersync.reload({stream: true}));
});

// Start Browser-Sync server
gulp.task('browsersync-server', function(){
	browsersync.init(null, {
		port: 3000,
		server: {baseDir: 'dist/'},
		open: true,
		notify: true,
		reloadOnRestart: true
	});
});

//
// WATCH TASK
//

gulp.task('watch', function(){
	gulp.watch(dev_path.pug + '**/*.pug', ['pug']);
	gulp.watch(dev_path.styl + '**/*.styl', ['stylus']);
	gulp.watch(dev_path.js + '**/*.js', ['js']);
	gulp.watch(dev_path.img + '**/*', ['img']);

	// gulp.watch([dev_path.styl + 'vendor/*', dev_path.js + 'vendor/*'], ['vendor']);
});

//
// DEFAULT TASK
//
gulp.task('default', [
	// 'vendor', 
	'pug', 'copy', 'stylus', 'js', 'browsersync-server', 'watch'
]);

gulp.task('prod', [
	'pug', 'copy', 'stylus', 'js'
]);